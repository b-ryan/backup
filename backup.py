#!/usr/bin/env python
from collections import namedtuple
import sys
from sqlite3 import connect
import fileinput
import click
import subprocess


def _file_md5(filename):
    stdout = subprocess.check_output(["md5sum", filename]).decode("utf-8")
    return stdout.split()[0]

BackupFile = namedtuple("BackupFile", ("fname", "md5", "status"))


def _get_curr_file(cur, fname):
    cur.execute("select fname, md5, status from files where fname = ?",
                (fname,))
    row = cur.fetchone()
    return BackupFile(*row) if row else None


def _upsert_file(cur, bfile: BackupFile):
    cur.execute("replace into files (fname, md5, status) values (?, ?, ?)",
                (bfile.fname, bfile.md5, bfile.status))


@click.command()
@click.argument("backup-file", type=click.File("r"))
@click.option("--database", required=True)
def register(backup_file, database):
    with connect(database, isolation_level=None) as conn:
        cur = conn.cursor()
        for line in backup_file:
            fname = line.strip()
            md5 = _file_md5(fname)
            curr_file = _get_curr_file(cur, fname)
            if curr_file and curr_file.md5 == md5:
                continue
            _upsert_file(cur, BackupFile(fname, md5, "pending"))


@click.group()
def cli():
    pass

cli.add_command(register)

if __name__ == "__main__":
    cli()
